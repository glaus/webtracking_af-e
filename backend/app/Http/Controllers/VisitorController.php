<?php

namespace App\Http\Controllers;

use App\User;
use App\Visitor;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Response;

class VisitorController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'cookie_value' => 'unique:tbl_visitor|max:32',
            'footprint_value' => 'unique:tbl_visitor|max:32',
        ]);
        $visitor = $request->all();
        return Visitor::create($visitor);
    }


    public function show(Request $request){
        $input = $request->all();
        if(array_key_exists('footprint_value',$input) && $input['footprint_value']){
            return Visitor::where('footprint_value', $input['footprint_value'])->first();

        }else if (array_key_exists('cookie_value',$input) && $input['cookie_value']) {
            return Visitor::where('cookie_value', $input['cookie_value'])->first();
        }
        else
        {
            return Visitor::get();
        }
    }


    public function updateByFootprint(Request $request){
        $this->validate($request, [
            'cookie_value' => 'max:32',
            'footprint_value' => 'exists:tbl_visitor,footprint_value'
        ]);
        $resource = Visitor::where('footprint_value','=',$request->only(['footprint_value']))->first();
        $resource->fill($request->only([
            'cookie_value'
        ]));
        if($resource->save()){
            return Response::json(array(
                'status'=>$resource,
                'msg'=> 'Visitor inserted'

            ));
        }else{
            abort(405,'Malformed Request');
        }
    }

    public function updateByCookie(Request $request){
        $this->validate($request, [
            'footprint_value' => 'max:32',
            'cookie_value' => 'exists:tbl_visitor,cookie_value'
        ]);
        $resource = Visitor::where('cookie_value','=',$request->only(['cookie_value']))->first();
        $resource->fill($request->only([
            'footprint_value'
        ]));
        if($resource->save()){
            return Response::json(array(
                'status'=>$resource,
                'msg'=> 'Visitor updated'

            ));
        }else{
            abort(405,'Malformed Request');
        }
    }


}
