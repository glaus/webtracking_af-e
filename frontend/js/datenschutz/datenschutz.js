(function(){
    'use strict';

    // Prepare the 'users' module for subsequent registration of controllers and delegates
    angular.module('datenschutz', [ 'ngMaterial' ])
        .config(homeRouteProvider);

    function homeRouteProvider($stateProvider) {
        $stateProvider
            .state("datenschutz", {
                views: {
                    "content" : {
                        templateUrl: "js/datenschutz/datenschutz.html",
                        controller: "datenschutzController"
                    }
                },
                url:"/datenschutz"
            });
    };



})();
