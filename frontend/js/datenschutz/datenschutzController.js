(function(){

    angular
        .module('datenschutz')
        .controller('datenschutzController', [
            '$scope',
            optInController
        ]);

    /**
     * Main Controller for the Angular Material Starter App
     * @param $scope
     * @param $mdSidenav
     * @param avatarsService
     * @constructor
     */
    function optInController($scope, Tracking) {
        var self = this;
		Tracking.addPage("fabianglaus.ch/tracker/#/history");
    }

})();
