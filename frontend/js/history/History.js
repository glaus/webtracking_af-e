(function(){
    'use strict';

    // Prepare the 'users' module for subsequent registration of controllers and delegates
    angular.module('history', [ 'ngMaterial' ])
        .config(homeRouteProvider);

    function homeRouteProvider($stateProvider) {
        $stateProvider
            .state("history", {
                views: {
                    "content" : {
                        templateUrl: "js/history/history.html",
                        controller: "HistoryController"
                    }
                },
                url:"/history"
            });
    };



})();
