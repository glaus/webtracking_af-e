(function(){

    angular
        .module('home')
        .controller('HomeController', [
            '$scope', 'Tracking',
            HomeController
        ]);

    /**
     * Main Controller for the Angular Material Starter App
     * @param $scope
     * @param $mdSidenav
     * @param avatarsService
     * @constructor
     */
    function HomeController($scope, Tracking) {
        var self = this;
        Tracking.addPage("fabianglaus.ch/tracker");
    }

})();
