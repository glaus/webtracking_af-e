(function(){
    'use strict';

    // Prepare the 'users' module for subsequent registration of controllers and delegates
    angular.module('optIn', [ 'ngMaterial' ])
        .config(homeRouteProvider);

    function homeRouteProvider($stateProvider) {
        $stateProvider
            .state("optIn", {
                views: {
                    "content" : {
                        templateUrl: "js/optIn/optIn.html",
                        controller: "optInController"
                    }
                },
                url:""
            });
    };



})();
